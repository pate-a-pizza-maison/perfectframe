extends Node2D

# Array of saved properties over time
var saved_tics = []
# Preview of a tic
var preview
# Default modulation for the preview. Can be changed in each instance obviously
var preview_modulate = Color(0.25,0.25,1,0.5)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init_save_array(sec):
	saved_tics.resize(sec)
	
func save_tic(_sec):
	# Do something to save the state of the object
	pass

func restore_tic(_sec):
	# Do something to restore the state of the object
	
	# What we want to do everytime, however, is erase the saved tics for the tics after the one restored
	for i in range(_sec,saved_tics.size()):
		saved_tics[i] = null
	pass
	
func preview_tic(_sec):
	# Do something to show a preview of the object
	pass
	
func stop_preview_tic():
	# Do something to remove the preview
	pass