* [ ] Une histoire
* [ ] Un tuto, une série de niveaux et un niveau de fin
* [ ] Un hub
 ---
* [ ] Un éditeur de niveau
* [ ] Un éditeur d'arbre de "decisions" pour chaque objet
* [ ] Un bonus stage chaos
* [ ] Un score
---
* [ ] Une notion d'incognito qui influe sur une jauge de théorie de complot globale
* [ ] Une continuité en fonction d'à quel point les missions ont été faites discrètement
* [ ] Des outils à débloquer et à ramener depuis un hub pour faire un niveau plus discrètement
* [ ] Différentes fins en fonction de la jauge de théorie du complot
* [ ] Des outils pour faire faire baisser la jauge d'incognito (hors de toute notion d'assassinat)
* [ ] Plusieurs moyens de faire une mission
* [ ] Un karma variant en fonction des actions réalisées qui t'es donné à a fin et qui te dit si tu es un enculé temporel ou pas
* [ ] Une méthode méga-chaotique pour chaque niveau ?
* [ ] Les actions et décisions sauvegardées en données sous forme d'arbre


Intro: un vaisseau dans l'espace dans le futur après que la terre ait explosé

Tuto: Buter Kennedy

Niveaux:

* Tuer Hitler
* Tuer

Fin: Se tuer soi-même

Objets du futur:

* Clone programmable pour faire des actions avec d'autres gens
* Flashouïeur


Interactions possibles:

* Mettre des cales à un camion (duh)
* Empoisonner une boisson (médicament, poison, produit chimique)
* Dévisser un truc (tournevis)
* Verrouiller une porte (utiliser une clé)
* Mettre un truc glissant à un endroit (peau de banane, huile)
* Mettre un trampoline à un endroit pour empêcher quelqu'un de sauter d'un toit (donc personne appelle les secours)
* Mettre un panier de chatons à un endroit
* Crever des pneus
* Aider une mamie à traverser
* Briser la chaine d'un chien méchant (utiliser une pince coupante)
* Retirer un réveil à quelqu'un qui doit se réveiller
* Mettre un réveil à quelqu'un pour qu'il se réveille plus tôt
* Augmenter le son d'un ampli (à 11)
* Retirer une plaque d'égout (pied de biche)
* Nourrir des mogwai après minuit ?
* Faire se déverser des produits radioactifs ?
* Provoquer le bordel dans une église (en remplaçant un morceau par du metal)
* Voler/rendre le café de quelqu'un pour qu'il s'endorme ou pas
* Vider un frigo pour forcer quelqu'un à faire les courses
* Réveiller les morts avec du matos de nécromancie
