Mécanique de jeu: On doit tuer une cible depuis à un endroit précis depuis un endroit précis avec notre fusil sniper. Il faut faire en sorte que au cours de la scène, la cible soit présente sur la position et que la ligne de tir soit dégagée. Pour cela, on navigue dans un intervalle de temps donné et on interagit avec des objets pour modifier le cours des évènement et rendre le tir possible.

Personnage: Assassin temporel

Outils:
* Sniper pour tuer UNE unique cible
* Montre pour naviguer dans le temps dans la scène et l'arrêter
* Inventaire permettant de transporter des objets à travers le temps

Contexte de jeu:
* Vue du dessus 3/4
