extends Node

# Current active scene (level or hub)
var current_scene
# The time manager of the scene
var time_manager : Node
# The gui of the scene
var gui : MarginContainer
# root of the tree, to avoid retyping 
var root

# Obsolete configuration warning for the Hub
func _get_configuration_warning() -> String:
	if current_scene.get_node("TimeManager") == null:
		return "The level need to have a TimeManager to work properly"
	else:
		return ""

# Called when the node enters the scene tree for the first time.
func _ready():
	root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	init_gui()

# Init the GUI
func init_gui():
	gui = current_scene.get_node("CanvasLayer/GUI")
	if gui != null:
		gui.connect("ready",self,"init_time_manager")

# Function called once the GUI is ready, sets the GUI for the TimeManager
func init_time_manager():
	time_manager = current_scene.get_node("TimeManager")
	if time_manager != null:
		time_manager.set_gui(gui)
	time_manager.init_bar()