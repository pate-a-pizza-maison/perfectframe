extends Panel

# Horizontal container
var h_container : HBoxContainer
# Currently lit tic
var current_tic : int = 1
# Modulated color when time is stopped
export var modulated_color_time_stop = Color(1,1,1)

# Signals

signal tic_is_clicked(tn)
signal tic_is_hovered(tn)
signal tic_stop_hovered()

func _ready():
	h_container = $MarginContainer/HBoxContainer

# Inits the bar with the right number of tics
func init_bar(tics):
	if h_container != null:
		for child in h_container.get_children():
			child.queue_free()
		var time_tic = preload("res://Scenes/Autoloads/GUI/TimeTicBar/TimeTic/TimeTic.tscn")
		for i in range(tics):
			var instanced_time_tic = time_tic.instance()
			h_container.add_child(instanced_time_tic)
			instanced_time_tic.set_number(i)
			instanced_time_tic.connect("tic_is_clicked",self,"tic_is_clicked")
			instanced_time_tic.connect("tic_is_hovered",self,"tic_is_hovered")
			instanced_time_tic.connect("tic_stop_hovered",self,"tic_stop_hovered")
			
# Updates the bar to light up the right square
func update_bar(tic_to_light):
	if tic_to_light >= 0 and tic_to_light < h_container.get_children().size() and current_tic != tic_to_light:
		h_container.get_child(current_tic).current = false
		h_container.get_child(tic_to_light).current = true
		current_tic = tic_to_light

# Toggle the stopped time bar color
func toggle_stopped_time():
	if modulate == modulated_color_time_stop:
		modulate = Color(1,1,1)
	else:
		modulate = modulated_color_time_stop
		
func tic_is_clicked(tn):
	emit_signal("tic_is_clicked",tn)
	
func tic_is_hovered(tn):
	emit_signal("tic_is_hovered",tn)

func tic_stop_hovered():
	emit_signal("tic_stop_hovered")
