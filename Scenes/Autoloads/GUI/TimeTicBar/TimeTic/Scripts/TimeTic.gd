extends Control

# TimeTic is the current TimeTic
export var current = false setget set_current
# ColorRect for the UI
var color_rect : ColorRect
# Tic number this tic represents
var tic_number : int 

# Signals

signal tic_is_clicked(tn)
signal tic_is_hovered(tn)
signal tic_stop_hovered()

func set_number(tn):
	tic_number = tn

# Called when the node enters the scene tree for the first time.
func _ready():
	color_rect = $ColorRect
	
func set_current(is_current):
	if is_current:
		color_rect.color = Color(1,0,0,color_rect.color.a)
	else:
		color_rect.color = Color(1,1,1,color_rect.color.a)
	current = is_current

# Called when the tic is pressed
func _on_Button_pressed():
	emit_signal("tic_is_clicked",tic_number)


func _on_Button_mouse_entered():
	emit_signal("tic_is_hovered",tic_number)


func _on_Button_mouse_exited():
	emit_signal("tic_stop_hovered")
