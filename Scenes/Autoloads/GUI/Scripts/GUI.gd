extends MarginContainer

# The bar that contains time tics
var time_tic_bar : Panel setget set_time_tic_bar, get_time_tic_bar

# Called when the node enters the scene tree for the first time.
func _ready():
	time_tic_bar = $"VBoxContainer/HBoxContainer/TimeTicBar"

func get_time_tic_bar() -> Panel:
	return time_tic_bar
	
func set_time_tic_bar(ttb):
	time_tic_bar = ttb
	
# Toggle the state of the ui when the time is stopped
func toggle_stopped_time():
	time_tic_bar.toggle_stopped_time()