extends Node

# Number of tics in the level
export var tics = 0 setget set_tics, get_tics
# Seconds elapsed since the start
var seconds = 0
# Full seconds elapsed
var full_second :int = -1
# GUI
var gui : MarginContainer setget set_gui
# Array of all entities for each second elapsed
var entities_log = []

var time_tic_bar : Panel

func _ready():
	entities_log.resize(tics)
	pass
	
func _physics_process(delta):
	var old_full_second = full_second
	full_second = int(seconds)
	seconds += delta
	gui.time_tic_bar.update_bar(full_second)
	if old_full_second != full_second:
		register_state(full_second)
	process_input()
	
func process_input():
	if Input.is_action_just_pressed("ui_stop_time"):
		if Engine.time_scale == 1:
			Engine.time_scale = 0
			gui.toggle_stopped_time()
		else:
			Engine.time_scale = 1
			gui.toggle_stopped_time()
	
func set_tics(t):
	tics = t if t>0 else 0
	
func get_tics() -> int:
	return tics

func set_gui(g):
	# Add the connects later
	gui = g
	time_tic_bar = gui.time_tic_bar
	time_tic_bar.connect("tic_is_clicked",self,"restore_tic")
	time_tic_bar.connect("tic_is_hovered",self,"preview_tic")
	time_tic_bar.connect("tic_stop_hovered",self,"stop_preview_tic")
	
# Initialization of the time tic bar
func init_bar():
	time_tic_bar.init_bar(tics)
	var entities = get_tree().get_nodes_in_group("Entity")
	for member in entities:
		member.init_save_array(tics)

# Register the state of all objects every second
func register_state(sec):
	var entities = get_tree().get_nodes_in_group("Entity")
	for member in entities:
		member.save_tic(sec)
		
func restore_tic(sec):
	print_debug("Restore the tic number ", sec)
	var entities = get_tree().get_nodes_in_group("Entity")
	for member in entities:
		member.restore_tic(sec)
	seconds = sec
	full_second = -1
		
func preview_tic(sec):
	print_debug("Previewing the tic number ", sec)
	var entities = get_tree().get_nodes_in_group("Entity")
	for member in entities:
		member.preview_tic(sec)
		
func stop_preview_tic():
	var entities = get_tree().get_nodes_in_group("Entity")
	for member in entities:
		member.stop_preview_tic()