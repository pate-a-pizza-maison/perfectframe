extends "res://classes/Entity.gd"

var animation_player : AnimationPlayer
var sprite: Sprite
var kb: KinematicBody

func _ready():
	animation_player = $KinematicBody/AnimationPlayer
	sprite = $KinematicBody/Sprite
	kb = $KinematicBody
	
func show():
	animation_player.play("open")
	
func hide():
	animation_player.play("close")
	
func activate():
	# Script triggered when activated by the player 
	pass
	
func deactivate():
	# Script triggered when deactivated by the player
	pass
	
func save_tic(sec):
	if sec < saved_tics.size():
		saved_tics[sec] = {"kbtransform" : kb.transform}

func restore_tic(sec):
	if saved_tics[sec] != null:
		kb.transform = saved_tics[sec]["kbtransform"]
	#We want to call the parent function after the treatment because it removes the frames
	.restore_tic(sec)
	
func preview_tic(sec):
	.preview_tic(sec)
	if saved_tics[sec] != null:
		preview = sprite.duplicate()
		preview.modulate = preview_modulate
		add_child(preview)
		preview.transform = saved_tics[sec]["kbtransform"]
		print_debug("og transform : ", transform)
		
func stop_preview_tic():
	.stop_preview_tic()
	if(is_instance_valid(preview)):
		preview.queue_free()