extends "res://classes/Entity.gd"

# Player movement speed in px/sec
export var movement_speed = 200
# Animation player
var animation_player : AnimationPlayer
# Sprite of the player
var sprite: Sprite

# The kinematic body
var kb

# Called when the node enters the scene tree for the first time.
func _ready():
	# Assign variables
	animation_player = $KinematicBody/AnimationPlayer
	sprite = $KinematicBody/Sprite
	kb = $KinematicBody
	# Play base animation
	animation_player.play("idle")
	
func _physics_process(delta):
	# Update de movement of the player on every frame
	update_movement()
	
# Function to update the movement of the player according to input
func update_movement():
	var direction = Vector2(0,0)
	if Input.is_action_pressed("ui_up"):
		direction.y -= 1
	if Input.is_action_pressed("ui_down"):
		direction.y += 1
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
	if Input.is_action_pressed("ui_jump"):
		animation_player.play("jump")
		animation_player.animation_set_next("jump","idle")
	if Input.is_action_pressed("ui_crouch"):
		animation_player.play("crouch")
		animation_player.animation_set_next("crouch","idle")
	
	# Flip the sprite according to the direction
	if direction.x != 0:
		sprite.flip_h = (direction.x == -1)
	# Normalize the vector before applying the speed to avoid going faster in diagonal movement
	direction = direction.normalized() * movement_speed
	if direction != Vector2.ZERO:
		if animation_player.current_animation != "walk":
			animation_player.play("walk")
	elif animation_player.current_animation == "walk":
			animation_player.play("idle")
		
	# Effectively move the player
	kb.move_and_slide(direction)

func save_tic(sec):
	if sec < saved_tics.size():
		saved_tics[sec] = {"kbtransform" : kb.transform}

func restore_tic(sec):
	if saved_tics[sec] != null:
		kb.transform = saved_tics[sec]["kbtransform"]
	#We want to call the parent function after the treatment because it removes the frames
	.restore_tic(sec)
	
func preview_tic(sec):
	.preview_tic(sec)
	if saved_tics[sec] != null:
		preview = sprite.duplicate()
		preview.modulate = preview_modulate
		add_child(preview)
		preview.transform = saved_tics[sec]["kbtransform"]
		print_debug("og transform : ", transform)
		
func stop_preview_tic():
	.stop_preview_tic()
	if(is_instance_valid(preview)):
		preview.queue_free()
